// Copyright © 2015 George King. Permission to use this file is granted in ploy/license.txt.


class HostVal: Form { // host value declaration: `host_val sym Type;`.
  let typeExpr: Expr
  let code: LitStr
  let deps: [Identifier]

  init(_ syn: Syn, typeExpr: Expr, code: LitStr, deps: [Identifier]) {
    self.typeExpr = typeExpr
    self.code = code
    self.deps = deps
    super.init(syn)
  }

  override func write<Stream : TextOutputStream>(to stream: inout Stream, _ depth: Int) {
    writeHead(to: &stream, depth)
    typeExpr.write(to: &stream, depth + 1)
    code.write(to: &stream, depth + 1)
    for dep in deps {
      dep.write(to: &stream, depth + 1)
    }
  }
}
